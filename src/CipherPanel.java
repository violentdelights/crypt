/*Creates panels specific to each Cipher type*/

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class CipherPanel extends JPanel
{
	static int s = 0;
	static boolean encrypt = true; //to replace 0's and 1's down below.
	private static JCheckBox Break;
	private static JPanel root, panel0, panel1, panel2;
	private static JLabel banner0, banner1, banner2, banner3;
	private static JButton encryptButton, decryptButton, back;
	private static JTextField encrypted, decrypted, key;
	
	public static Component ShiftCipher()		//Shift Cipher panel
	{
		s = 0;
		root = new JPanel();
		
		encryptButton = new JButton("Encrypt " + (char)8659); //(char)8659 is the unicode character 'down arrow'
		decryptButton = new JButton("Decrypt " + (char)8657); //(char>8657 is the unicode character 'up arrow'
		Break = new JCheckBox(" Break");
		back = new JButton("Back");
		banner0 = new JLabel ("Original Text: ");
		banner1 = new JLabel ("Encrypted Text: ");
		banner2 = new JLabel ("Key: ");
		encrypted = new JTextField();
		decrypted = new JTextField();
		key = new JTextField();
		
		panel0 = new JPanel();
		panel0.add(banner0);
		panel0.add(decrypted);
		panel0.setLayout(new GridLayout(2, 1));
		
		panel1 = new JPanel();
		panel1.add(banner2);
		panel1.add(key);
		panel1.add(encryptButton);
		panel1.add(decryptButton);
		panel1.add(Break);
		panel1.setLayout(new GridLayout(1, 5));
		
		panel2 = new JPanel();
		panel2.add(banner1);
		panel2.add(encrypted);
		panel2.setLayout(new GridLayout(2, 1));
		
		root.add(panel0);
		root.add(panel1);
		root.add(panel2);
		root.add(back);
		root.setLayout(new GridLayout(4, 1));
		
		ButtonListener button = new ButtonListener();
		encryptButton.addActionListener(button);
		decryptButton.addActionListener(button);
		back.addActionListener(button);
		
		return root;
	}
	
	public static Component AffineCipher()		//Affine Cipher panel
	{
		root = (JPanel) ShiftCipher();
		
		key.setText("( , )");
		s = 1;
		
		return root;
	}
	
	static class ButtonListener implements ActionListener
	{
	    public void actionPerformed (ActionEvent event)
	    {
	    	if(event.getSource() == back)
	    	{
	    		root.removeAll();
	    		
	    		Panel window = new Panel();
	    		root.add(window);
	    		
	    		root.revalidate();
	    		root.repaint();
	    	}
	    	
	    	if(s == 0)
	    	{
		    	if(event.getSource() == encryptButton)
		        {
		    		encrypted.setText(Cipher.Shift(decrypted.getText(), key.getText(), 1, Break.isSelected()));
		        }
		    	
		    	if(event.getSource() == decryptButton)
		        {
		    		decrypted.setText(Cipher.Shift(encrypted.getText(), key.getText(), 0, Break.isSelected()));
		        }
	    	}
	    	
	    	if(s == 1)
	    	{
		    	if(event.getSource() == encryptButton)
		        {
		    		encrypted.setText(Cipher.Affine(decrypted.getText(), key.getText(), 1, Break.isSelected()));
		        }
		    	
		    	if(event.getSource() == decryptButton)
		        {
		    		decrypted.setText(Cipher.Affine(encrypted.getText(), key.getText(), 0, Break.isSelected()));
		        }
	    	}
	    } 
	}
} 