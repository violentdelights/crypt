/**
 * Contains methods that implement the basic Ciphers in MAT 447, Cryptography at Arizona State University.
 *  
 * @author Kevin Lough and Luke Mains
 * @version 1.0 21 Aug 2017
 * 
 */
public class Cipher	
{	
	/**
	 *
	 * @param text 
	 * @param key
	 * @param tag
	 * @param checked
	 * @return
	 */
	public static String Affine(String text, String key, int tag, boolean checked) 
	{
		int x, y;
		int[] numConvert = new int[text.length()];
		numConvert = Methods.toNum(text);
		String[] xy = key.split(","); 			//Change xy to "message"?
		xy[0] = xy[0].replaceAll("[^0-9]", "");
		xy[1] = xy[1].replaceAll("[^0-9]", "");
		
		if(!xy[0].isEmpty() && !xy[1].isEmpty())
		{
			x = Integer.parseInt(xy[0]);
			y = Integer.parseInt(xy[1]);
		}
		
		else
		{
			x = 2;
			y = 26;
		}
		
		if(checked && tag == 0)			//Forcefully breaks encryption
		{
			//********************************************************
		}
		
		if(0 <= y && y <= 25 && Methods.GCD(x, 26) == 1)
		{		
			if(tag == 0)	//Decrypt
			{
				int m = 0;
				for(int i = 0; i < 25; i++)
				{
					if((x * i) % 26 == 1)
					{
						m = i;
						break;
					}
				}
				
				for(int i = 0; i < numConvert.length; i++)
				{
					if(numConvert[i] < y)
						numConvert[i] = (m* (numConvert[i]+(26-y))) % 26;
					else
						numConvert[i] = (m * (numConvert[i] - y)) % 26;
				}
				
			}
			
			if(tag == 1)	//Encrypt
			{
				for(int i = 0; i < numConvert.length; i++)
				{
					numConvert[i] = (x * numConvert[i] + y) % 26;
				}
			}
		}
		
		else 
		{
			text = "Invalid Key: Please enter an x s.t gcd(x, 26) = 1 and y is between 0 and 25";
			return text;
		}
		
		text = Methods.toString(numConvert);
		
		return text;
	}    

	public static String Shift(String text, String key, int tag, boolean checked)
	{
		int[] numConvert = new int[text.length()];
		numConvert = Methods.toNum(text);
		key = key.replaceAll("[^0-9]", "");
		
		if(key.isEmpty())
		{
			key = "26";
		}	
		int shift = Integer.parseInt(key);
		
		if(checked && tag == 0)			//Forcefully breaks encryption
		{
			int[] scores = new int[26];
			char[] letters = new char[numConvert.length];
			int[] temp = new int[numConvert.length];
			
			for(shift = 0; shift < 26; shift++)
			{
				for(int i = 0; i < numConvert.length; i++)
				{
					if(numConvert[i] < shift)
						temp[i] = (numConvert[i]+(26-shift)) % 26;
					else
						temp[i] = (numConvert[i]-shift) % 26;
				}
				
				letters = (Methods.toString(temp)).toCharArray();
				scores[shift] = Methods.isEnglish(letters);
			}
			
			int max = scores[0];
			
			for(int i = 0; i < 26; i++)
			{
				if(scores[i] > max)
				{
					max = scores[i];
					shift = i;
				}
			}
		}
		
		if(0 <= shift && shift <= 25)
		{		
			if(tag == 0)	//Decrypt
			{
				for(int i = 0; i < numConvert.length; i++)
				{
					if(numConvert[i] < shift)
						numConvert[i] = (numConvert[i]+(26-shift)) % 26;
					else
						numConvert[i] = (numConvert[i]-shift) % 26;
				}
			}
			
			if(tag == 1)	//Encrypt
			{
				for(int i = 0; i < numConvert.length; i++)
				{
					numConvert[i] = (numConvert[i]+shift) % 26;
				}
			}
		}
		
		else 
		{
			text = "Invalid Key: Please enter a key between 0 and 25";
			return text;
		}
		
		text = Methods.toString(numConvert);
		
		if(checked)
			text = "(" + shift + ") :" + text;
		
		return text;
	}    
} 