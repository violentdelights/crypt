/*Creates the applet container*/

import javax.swing.*;

public class Applet extends JApplet
{
	public void init()
	{
		Panel window = new Panel();
		getContentPane().add(window);
		
		setSize (600, 250);
	}
}