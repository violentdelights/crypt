public class ShiftCipher{
	private int shiftKey;
	
	public ShiftCipher(int shiftKey){
		this.shiftKey = shiftKey;
	}
	
	public void Convert(String message){
		int[] numbers = new int[message.length()];
		
		for(int i = 0; i < message.length(); i++){
			numbers[i] = message.charAt(i);
		}
	}
	
	public void Encode(int[] numbers, int[] encodedMessage){
		for(int i = 0; i < numbers.length; i++){
			encodedMessage[i] = numbers[i] + shiftKey;
		}
	}
	
	public void Decode(int[] numbers, int[] decodedMessage){
		for(int i = 0; i < numbers.length; i++){
			decodedMessage[i] = numbers[i] - shiftKey;
		}
	}
	
}
