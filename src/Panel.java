/*Creates the Cipher selection panel*/

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Panel extends JPanel
{
	private JLabel banner;
	private JPanel panel;
	private JButton select;
	private JComboBox<String> options; 
	String[] ciphers = {"Shift", "Affine"};		//Cipher options, add as more are coded

	public Panel()
	{
		banner = new JLabel ("Select Encryption Type: ");
		select = new JButton ("select");
		options = new JComboBox<String>();
		
		for(int i = 0; i < ciphers.length; i++)
		{
			options.addItem(ciphers[i] + " Cipher");
		}
		
		panel = new JPanel();
		panel.add(banner);
		panel.add(options);
		panel.add(select);
		panel.setLayout(new GridLayout(3, 1));

		ButtonListener button = new ButtonListener();
		select.addActionListener(button);
		add(panel); 
	}
	
	class ButtonListener implements ActionListener
	{
	    public void actionPerformed (ActionEvent event)
	    {
	    	if(event.getSource() == select)
	        {
	    		String s = (String) options.getSelectedItem();
	  
	    		panel.removeAll();
	    		
	    		switch (s) 
	    		{
	    			case "Shift Cipher":						//dropdown selection
	    				add(CipherPanel.ShiftCipher());
	    				break;
	    			case "Affine Cipher":						//dropdown selection
	    				add(CipherPanel.AffineCipher());
		    			break;	
	    		}
	    		
	    		revalidate();
	    		repaint();
	        }
	    } 
	}    
} 